import React from 'react';
import axios from 'axios';


class products extends React.Component {
    constructor() {
        super();
        this.state = {
            rest: []
        }
    }
    componentDidMount() {
        console.log("componentDidMount");

        axios.get('http://127.0.0.1:8000/products/').then(res => {
            const rest = res.data;
            this.setState({ rest });
            console.log(res.data)
        });


    }
    render() {
        return (
            <div className='container'>
                <br />
                <div className="row" id="ads">

                    {this.state.rest.map((p, index) => (
                        <div className="col-md-4" key={index}>
                            <div className="card rounded" >
                                <div className="card-image">
                                    <h3 className="card-notify-badge bg-light text-center ">{p.title}</h3>
                                    <img className="img" style={{
                                        width: "100%",
                                        height: "300px"

                                    }}
                                        src={p.image} />

                                </div>
                                <div className="card-image-overlay m-auto">
                                </div>
                                <div className="card-body text-center">
                                    <span className="badge badge-primary">rating : {p.rating}</span><br/>
                                    <span className="text-muted">description : {p.description}</span>

                                </div>
                                <div className="card-footer">
                                    <span className="card-detail-badge"><b className="badge badge-danger"> price : Rs{p.price}/-</b></span>
                                    <span className="card-detail-badge"><b className="badge badge-info"> dicount : Rs{p.discount_price}/-</b></span>

                                    <button className="btn btn-sm btn-success btn-block" href="#">buy now</button>
                                    <button className="btn btn-sm btn-warning btn-block" href="#">add to cart</button>

                                </div>
                            </div>
                        </div>
                    ))}
                </div>

            </div>


        );
    }
}
export default products;
