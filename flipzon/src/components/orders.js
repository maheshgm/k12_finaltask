import React from 'react';
// import axios from 'axios';
import axiosInstance from '../axios';


class orders extends React.Component {
    constructor() {
        super();
        this.state = {
            rest: []
        }
    }
    componentDidMount() {
        console.log("componentDidMount");
        // const baseURL = 'http://127.0.0.1:8000/';
        axiosInstance.get('orders/').then(res => {
            const rest = res.data;
            this.setState({ rest });
            console.log(res.data)
        });


    }
    render() {
        return (
            <div className='container'>
                <br />
                <div className="row" id="ads">

                    {this.state.rest.map((p, index) => (
                        <div className="col-md-4" key={index}>
                            <div className="card rounded" >
                                <div className="card-image">
                                    
                                </div>
                                <div className="card-image-overlay m-auto">
                                </div>
                                <div className="card-body text-center">
                                    <span className="badge badge-primary">payment_method : {p.payment_method}</span><br/>
                                 
                                </div>
                                <div className="card-footer">
                                    <span className="card-detail-badge"><b className="badge badge-danger"> ord_product : {p.ord_product}/-</b></span>
                                    <span className="card-detail-badge"><b className="badge badge-info"> ord_created_at : Rs{p.ord_created_at}/-</b></span>

                                   
                                </div>
                            </div>
                        </div>
                    ))}
                </div>

            </div>


        );
    }
}
export default orders;
