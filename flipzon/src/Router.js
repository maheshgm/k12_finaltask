
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import React from 'react';

import NotFoundPage from './NotFoundPage';
import products from './components/products';


const Routes = () => {
    return (

        <Router>
            <div>
                <nav class="navbar navbar-inverse">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" ><Link to="/products">FlipZon</Link></a>
                        </div>
                        <ul class="nav navbar-nav">
                            <li className="nav-item">
                                <a className="nav-link" >  <Link to="/Home">Home</Link></a>

                            </li>

                            <ul class="nav navbar-nav navbar-right">
                                <li><a className="nav-link"><span class="glyphicon glyphicon-user"></span> <Link to="/Home">Sign Up</Link></a></li>
                                <li><a className="nav-link"><span class="glyphicon glyphicon-log-in"></span><Link to="/Home">Log in</Link></a></li>
                            </ul>
                        </ul>

                    </div>
                </nav>
                <Switch>
                    <Route exact path="/products"> <products /></Route>
                    <Route path="/*"> <NotFoundPage /> </Route>


                </Switch>

            </div>
        </Router>

    );
}
export default Routes; 
