
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password

from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from rest_framework.authtoken.models import Token

from products.models import *

class UserSerializer(ModelSerializer):

    class Meta:
        model = User
        fields = '__all__'

class ProductSerializer(ModelSerializer):
    image_url = serializers.SerializerMethodField('get_image_url')

    class Meta:
        model = Product
        fields = ('id','title','price','image','image_url','discount_price','label','category','description','rating')
        # depth = 1

    def get_image_url(self, obj):
        return obj.image.url

class OrderSerializer(ModelSerializer):
    order_by_user = UserSerializer(read_only=True, many=True)
    order_product = ProductSerializer(read_only=True,many=True)

    class Meta:
        model = Orders
        fields = '__all__'
        # fields = ('id','order_by_user','order_product','date','status','payment_method','quantity')
        depth = 1
    # def get_total(self,obj):
    #     total_price = obj.quantity * obj.order_product.price
    #     return total_price
    #
    # def create(self, validated_data):
    #     order_product = validated_data.pop('order_product')
    #     order = Orders.objects.create(**validated_data)
    #     for order_data in order_product:
    #         Product.objects.create(title=order, **order_data)
    #     return order
    #
    # def update(self, instance, validated_data):
    #     orders_prod = validated_data.pop('order_product')
    #     products = (instance.order_product).all()
    #     products = list(products)
    #     instance.product = validated_data.get('product', instance.product)
    #     instance.save()
    #
    #     for order_prod in orders_prod:
    #         product = products.pop(0)
    #         product.title = order_prod.get('title', product.rating)
    #         product.save()
    #     return instance

class OrderItemsSerializer(ModelSerializer):
    all_orders = OrderSerializer(read_only=True,many=True)
    item_order_by_user = UserSerializer(read_only=True, many=True)
    class Meta:
        model = OrderItems
        fields = '__all__'
        # fields = ('item_order_by_user','all_orders','quantity')
        depth = 1

class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )

    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'password2')
        # extra_kwargs = {
        #     'first_name': {'required': True},
        #     'last_name': {'required': True}
        # }

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})

        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            # first_name=validated_data['first_name'],
            # last_name=validated_data['last_name']
        )

        user.set_password(validated_data['password'])
        user.save()

        return user

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'password']
        extra_kwargs = {'password': {'write_only': True, 'required': True}}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        Token.objects.create(user=user)
        return user

