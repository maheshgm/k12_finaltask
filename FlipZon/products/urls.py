from products import views
from django.urls import path
from rest_framework.views import APIView
from rest_framework_simplejwt.views import TokenRefreshView, TokenObtainPairView

urlpatterns = [
    # login
    # path('account/login/', obtain_jwt_token),
    # path('account/login/refresh/', refresh_jwt_token),
    # path('account/logout/', views.LogoutView.as_view(), name='auth_logout'),
    # path('account/register/', account_view.RegisterApi.as_view(),name='auth_register'),
    # path('jwt_verify/', verify_jwt_token),

    # path('change_password/<int:pk>/', ChangePasswordView.as_view(), name='auth_change_password'),
    # path('update_profile/<int:pk>/', UpdateProfileView.as_view(), name='auth_update_profile'),


    # RestApiViews

    path('accounts/login/', TokenObtainPairView.as_view()),
    path('accounts/login/refresh/', TokenRefreshView.as_view()),
    path('account/logout/', views.LogoutView.as_view()),
    path('accounts/register/', views.RegisterView.as_view()),

    path('products/', views.ProductListApi.as_view()),
    path('product/<int:id>', views.ProductDetails.as_view()),

    path('orders/', views.OrderList.as_view()),
    path('order/<int:id>', views.OrderDetails.as_view()),

    path('orderItems/', views.OrderItemsList.as_view()),
    path('orderItem/<int:id>', views.OrderItemsRetrive.as_view()),

]

