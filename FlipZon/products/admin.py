from django.contrib import admin
from django.utils.html import format_html
from products.models import Product, Orders, OrderItems


class PruductAdmin(admin.ModelAdmin):
	def thumbnail(self, object):
		return format_html('<img src="{}" width="50px" style="border-radius:50%;"/>'.format(object.image.url))

	list_display_links = ['thumbnail', 'title', 'category']
	list_display = ('title', 'price', 'discount_price','thumbnail','category','rating','prd_created_at','prd_updated_at')

class OrderAdmin(admin.ModelAdmin):
	list_display = ('user_id','ord_created_at','ord_updated_at','payment_method','status' ,'quantity')

class OrderItemsAdmin(admin.ModelAdmin):
	list_display = ('user_id','order_id','product_id','total_price')

admin.site.register(Product,PruductAdmin)
admin.site.register(Orders,OrderAdmin)
admin.site.register(OrderItems,OrderItemsAdmin)
