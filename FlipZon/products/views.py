from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.views import APIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from products.models import Product, Orders,OrderItems
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.response import Response
from products.serializer import RegisterSerializer, ProductSerializer, OrderSerializer, OrderItemsSerializer
from rest_framework.generics import ListAPIView, RetrieveAPIView, RetrieveUpdateDestroyAPIView, ListCreateAPIView, \
    CreateAPIView

# Create your views here.
class ProductListApi(ListAPIView):

    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    search_fields = ('title','price','label','id')
    ordering_fields=('title','id')

    # authentication_classes = [JSONWebTokenAuthentication, ]
    # permission_classes = [IsAuthenticated, ]

class ProductDetails(RetrieveAPIView):

    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    lookup_field = 'id'

class OrderItemsList(ListCreateAPIView):

    def get_queryset(self):
        return OrderItems.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    # queryset = OrderItems.objects.all()
    serializer_class = OrderItemsSerializer
    authentication_classes = [JSONWebTokenAuthentication, ]
    permission_classes = [IsAuthenticated]

class OrderItemsRetrive(RetrieveAPIView):

    def get_queryset(self):
        return OrderItems.objects.filter(user=self.request.user)

    # queryset = OrderItems.objects.all()
    serializer_class = OrderItemsSerializer
    lookup_field = 'id'
    authentication_classes = [JSONWebTokenAuthentication, ]
    permission_classes = [IsAuthenticated]

class OrderList(ListCreateAPIView):

    def get_queryset(self):
        return Orders.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        return serializer.save(user=self.request.user)

    # queryset = Orders.objects.all()
    serializer_class = OrderSerializer
    authentication_classes = [JSONWebTokenAuthentication, ]
    permission_classes = [IsAuthenticated]

class OrderDetails(RetrieveUpdateDestroyAPIView):
    authentication_classes = [JSONWebTokenAuthentication, ]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Orders.objects.filter(user=self.request.user)

    # queryset = Orders.objects.all()
    serializer_class = OrderSerializer
    lookup_field = 'id'

    # def partial_update(self, request, *args, **kwargs):
    #     kwargs['partial'] = True
    #     return self.update(request, *args, **kwargs)


    # #
    # def perform_update(self, request,serializer):
    #     instance = self.get_object()
    #     instance.id = request.data.get("id")
    #     instance.save()
    #     serializer = self.get_serializer(instance)
    #     serializer.is_valid(raise_exception=True)
    #     self.perform_update(serializer)
    #     return Response(serializer.data)




class RegisterView(CreateAPIView):
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RegisterSerializer

class LogoutView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        try:
            refresh_token = request.data["refresh_token"]
            token = RefreshToken(refresh_token)
            token.blacklist()

            return Response(status=status.HTTP_205_RESET_CONTENT)
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)

