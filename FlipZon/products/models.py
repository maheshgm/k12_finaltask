from django.db import models
from django.contrib.auth.models import User
from django.db.models import Sum,Count

CATEGORY_CHOICES = (
    ('fashion', 'Fashion'),
    ('electronics', 'Electronics')
)

PAYMENT_CHOICES = (
    ('COD', 'Cash On Delivery'),
    ('CARD', 'Card Pay'),
    ('UPI', 'UPI payment')
)
STSTUS = (
    ('PAID','Paid'),
    ('PENDING','Pending')
)
LABEL_CHOICES = (
    ('primary', 'Primary'),
    ('secondary', 'Secondary'),
    ('danger', 'Danger')
)

class Product(models.Model):
    title = models.CharField(max_length=100)
    price = models.FloatField()
    discount_price = models.FloatField(blank=True, null=True)
    label = models.CharField(choices=LABEL_CHOICES, max_length=20)
    category = models.CharField(choices=CATEGORY_CHOICES, max_length=20)
    rating = models.IntegerField(default=1)
    description = models.TextField()
    prd_created_at = models.DateTimeField(auto_created=True)
    prd_updated_at = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to='items/images/')

    def __str__(self):
        return self.title

class Orders(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='order_by_user')
    ord_product = models.ManyToManyField(Product, related_name='order_product')
    ord_created_at = models.DateTimeField(auto_created=True)
    ord_updated_at = models.DateTimeField(auto_now_add=True)
    payment_method = models.CharField(choices=PAYMENT_CHOICES,max_length=30, default='')
    status = models.CharField(choices=STSTUS,max_length=20)
    quantity = models.IntegerField(default=1)


    def __str__(self):
        return f"{self.payment_method}"
    @property
    def get_total(self):
        total_price = self.quantity * self.product.price
        return total_price

class OrderItems(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE,related_name='item_order_by_user')
    order_id = models.ForeignKey(Orders,on_delete=models.CASCADE,related_name='all_orders')
    product_id = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='order_item_product',null=True,
                                   blank=True)
    quantity = models.IntegerField()
    total_price = models.FloatField(null=True)

    def __str__(self):
        return f"{self.user_id} - {self.order_id} - {self.quantity}"
